const express = require('express');
const productCtrl = require('../controllers/product-controller');

const Router = express.Router();

Router.get('/',productCtrl.index) // api.com/product/ Index: Listar todos los productos
    .get('/version', productCtrl.showVersion)
    .post('/',productCtrl.create)   // api.com/product/ Create: Crear un nuevo producto
    .get('/:key/:value',productCtrl.find,productCtrl.show)    // api.com/product/category/Hogar Show: Muestra un producto en específico
    .put('/:key/:value',productCtrl.find,productCtrl.update)    // api.com/product/name/SamsungGalaxy Update: Actualizar un producto en especifico
    .delete('/:key/:value',productCtrl.find,productCtrl.remove);// api.com/product/name/SamsungGalaxy

module.exports = Router;
